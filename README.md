# pong.love

[![0 dependencies!](https://0dependencies.dev/0dependencies.svg)](https://0dependencies.dev)

An example repo that shows a simple game of Pong -- with all the tools needed
to read and modify its inner workings.

## Getting started

Install [LÖVE](https://love2d.org). It's just a 5MB download, open-source and
extremely well-behaved.

Download this repo:

```
git clone https://git.sr.ht/~akkartik/pong.love pong
```

Run the game:
```
love pong
```

When you want to read or modify the game, open up its editor by pressing
`ctrl+e`. (Check out more keyboard shortcuts below to see what you can do.)

When you're done reading or making changes, press `ctrl+e` again to play Pong
with any changes you made.

## Keyboard shortcuts: playing Pong

At the start, press any key to start a game. Once a game starts:

* `a`/`z` to move the left-hand paddle
* `up`/`down` to move the right-hand paddle
* `space` to start a new game

* `ctrl+e` to stop playing and browse the code for the game

## Keyboard shortcuts: reading source code for Pong

* `ctrl+e` to go back to playing Pong, using any changes you made

* `ctrl+g` to switch to a different file
* `ctrl+l` to toggle a second editor on the right for recent logs. Useful when
  debugging.

* `ctrl+=` to zoom in, `ctrl+-` to zoom out, `ctrl+0` to reset zoom

On the left (source code) side:
* `ctrl+f` to find patterns within the current file
* `ctrl+z` to undo, `ctrl+y` to redo
* `alt+right`/`alt+left` to jump to the next/previous word, respectively
* mouse drag or `shift` + movement to select text, `ctrl+a` to select all
* `ctrl+e` to modify the sources

Exclusively tested so far with a US keyboard layout. If
you use a different layout, please let me know if things worked, or if you
found anything amiss: http://akkartik.name/contact

## Debugging Pong

The primary method here to understand what Pong is doing (or indeed any other
program you choose to turn this repo into) is the log. To emit objects to the
log, use the `log` function:

```
log(2, "log message")
```

`log` takes exactly 2 arguments: a stack frame index to report the file and
line number for, and an object to log.

The stack frame index should be `2` if you call `log` directly. If you create
higher levels of abstraction around `log`, increment the stack frame
appropriately. Helper `f` calling `log` should use a stack frame index of `3`
to report line numbers from its caller. Helper `g` calling `f` calling `log`
should use a stack frame index of `4`, and so on.

The `log` function can only emit a single object at a time. However, the
object can be of any type.

Since each entry/line in the log contains a single object (after the filename
and line number), it's easy to specify how different objects should be
rendered. Just make sure the objects contain a field called `name`, and create
a function `log_render.name` to render it. In this repo, the function
`log_render.state` is an example of such a function, specifying how to render
a Pong state consisting of a ball position, ball velocity and paddle
positions.

Rendering functions must all have the same interface as
`log_render.state`:
```
function log_render.state(state, x,y, w)
  ...
end
```

The first argument is the object that was logged, the remaining arguments
specify the starting position from which to render it, and the width
on screen available to it. After the function finishes rendering the object,
it should return the y coordinate it drew until, so that the log browser knows
where to start rendering the next object.

## Sharing your changes

While pong.love lets you make changes to its source and shows how your changes
affect its behavior, the changes don't live in the same place as the original
sources. They live in the app's _save directory_
([love.filesystem.getSaveDirectory()](https://love2d.org/wiki/love.filesystem.getSaveDirectory)).

To distribute your version you'll need to copy the modified files over into
the app, either its directory or its .love zip archive. This process is
[unfortunately not automated yet](See https://love2d.org/wiki/Game_Distribution).

## Known issues

* ...

## Mirrors and Forks

This repo is a fork of [lines.love](http://akkartik.name/lines.html), an
editor for plain text where you can also seamlessly insert line drawings.
Its immediate upstream is [text.love](https://git.sr.ht/~akkartik/text.love),
a version without support for line drawings. Updates to it can be downloaded
from:

* https://git.sr.ht/~akkartik/pong.love
* https://tildegit.org/akkartik/pong.love
* https://git.merveilles.town/akkartik/pong.love
* https://codeberg.org/akkartik/pong.love
* https://nest.pijul.com/akkartik/pong.love (using the Pijul version control system)

Further forks are encouraged. If you show me your fork, I'll link to it here.

## Feedback

[Most appreciated.](http://akkartik.name/contact) Messages, PRs, patches,
forks, it's all good.
