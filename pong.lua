-- Pong
pong = {}
function pong.initialize_globals()
	N = 99
	M = math.floor(N/2)
end

function pong.initialize(arg)
	love.graphics.setBackgroundColor(1,1,1)
	log_new('session')
	App.screen.width, App.screen.height = N*3, N*3  -- the court
	App.screen.resize(App.screen.width, App.screen.height)
	if Settings and Settings.pong then
		pong.load_settings()
	else
		if Settings == nil then Settings = {} end
		if Settings.pong == nil then Settings.pong = {} end
		Settings.pong.x, Settings.pong.y, Settings.pong.displayindex = love.window.getPosition()
	end
	love.window.setTitle('pong.love')
	pong.new_game()
end

function pong.new_game()
	Ballp = {x=0, y=0}
	Ballv = {x=love.math.random()-0.5, y=love.math.random()-0.5}
	A = {ymin=-5, ymax=5}  -- at x=-M
	B = {ymin=-5, ymax=5}  -- at x=+M
	Pw, Pv = 3, 5  -- paddle width, velocity
	Game_start = false
	Game_over = false
	log_new('game')
	log_start('trajectory')
end

function pong.draw()
	love.graphics.scale(3, 3)
	love.graphics.translate(M, M)
	love.graphics.setColor(0,0,0)
	love.graphics.circle('fill', Ballp.x, Ballp.y, 3)
	love.graphics.rectangle('fill', -M, A.ymin, Pw, A.ymax-A.ymin)
	love.graphics.rectangle('fill', M-Pw+1, B.ymin, Pw, B.ymax-B.ymin)
end

function pong.update(dt)
	if not Game_start then return end
	if Game_over then return end
	Ballp.x = Ballp.x + Ballv.x
	Ballp.y = Ballp.y + Ballv.y
	if Ballp.y >= M or Ballp.y <= -M then
		Ballv.y = -Ballv.y
		log_state()
		log_new('trajectory')
	end
	if Ballp.x <= -M then
		if Ballp.y >= A.ymin and Ballp.y <= A.ymax then
			Ballv.x = -Ballv.x
			log_state()
			log_new('trajectory')
		else
			Game_over = true
			log_end('game')
		end
	end
	if Ballp.x >= M then
		if Ballp.y >= B.ymin and Ballp.y <= B.ymax then
			Ballv.x = -Ballv.x
			log_state()
			log_new('trajectory')
		else
			Game_over = true
			log_end('game')
		end
	end
	log_state()
end

function pong.keychord_press(chord, key)
	Game_start = true
	if chord == 'space' then
		pong.new_game()
	end
	if Game_over then return end
	if chord == 'down' then
		B.ymin, B.ymax = B.ymin+Pv, B.ymax+Pv
		log(2, 'B down')
	end
	if chord == 'up' then
		B.ymin, B.ymax = B.ymin-Pv, B.ymax-Pv
		log(2, 'B up')
	end
	if chord == 'a' then
		A.ymin, A.ymax = A.ymin-Pv, A.ymax-Pv
		log(2, 'A up')
	end
	if chord == 'z' then
		A.ymin, A.ymax = A.ymin+Pv, A.ymax+Pv
		log(2, 'A down')
	end
end

function pong.settings()
	if Current_app == 'pong' then
		Settings.pong.x, Settings.pong.y, Settings.pong.displayindex = love.window.getPosition()
	end
	return {
		x=Settings.pong.x, y=Settings.pong.y, displayindex=Settings.pong.displayindex,
	}
end

function pong.load_settings()
	local settings = Settings.pong
--? 	print('loading position', settings.x, settings.y)
	love.window.setPosition(settings.x, settings.y-37, settings.displayindex)
--? 	local x,y = love.window.getPosition()
--? 	print('after setposition', x,y)
end

-- vim:noexpandtab
